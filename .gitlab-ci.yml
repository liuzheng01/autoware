stages:
  - build
  - deploy

variables:
  ROS_CI_DESKTOP: "`lsb_release -cs`"
  CI_SOURCE_PATH: $CI_PROJECT_DIR
  GIT_SUBMODULE_STRATEGY: recursive
  VCS_FILE: autoware.ai.repos
  #CI_DEBUG_TRACE: "true"
  AUTOWARE_DOCKER_DATE: "latest"
  AUTOWARE_CROSS_TARGET_PLATFORM: generic-aarch64

.build: &build_common
  before_script:
    - sudo apt-get update
    # Use UTC for tzdata
    - DEBIAN_FRONTEND=noninteractive sudo apt-get install -y tzdata
    - sudo apt-get install -y gnupg2 lsb-release gcc g++ dpkg python-rosdep
    - sudo apt-get install -y python3-pip python3-setuptools python3-vcstool python3-colcon-common-extensions
    # Update setuptools from PyPI because the version Ubuntu ships with is too old
    - pip3 install -U setuptools
    - mkdir src
    - vcs validate < $VCS_FILE
    - vcs import src < $VCS_FILE
    - rosdep update
    - rosdep install -y --from-paths src --ignore-src --rosdistro ${ROS_DISTRO}

.build_cross_vars: &build_cross_vars
    AUTOWARE_TARGET_ARCH: aarch64
    AUTOWARE_TOOLCHAIN_FILE_PATH: $CI_PROJECT_DIR/ros/cross_toolchain.cmake
    AUTOWARE_SYSROOT: /sysroot/${AUTOWARE_CROSS_TARGET_PLATFORM}

.build_cross_script: &build_cross_script
  script:
    - apt-get update && apt-get install -y python3-vcstool git build-essential
    - mkdir src
    - vcs validate < $VCS_FILE
    - vcs import src < $VCS_FILE
    - source ${AUTOWARE_SYSROOT}/opt/ros/${ROS_DISTRO}/setup.bash
    - colcon build
        --merge-install
        --packages-skip citysim
        --cmake-args
        -DPYTHON_EXECUTABLE=/usr/bin/python3
        -DCMAKE_TOOLCHAIN_FILE=${AUTOWARE_TOOLCHAIN_FILE_PATH}
        -DCMAKE_SYSTEM_PROCESSOR=${AUTOWARE_TARGET_ARCH}
        -DCMAKE_PREFIX_PATH="${AUTOWARE_SYSROOT}/opt/ros/${ROS_DISTRO};${CI_PROJECT_DIR}/install"
        -DCMAKE_FIND_ROOT_PATH=${CI_PROJECT_DIR}/install

build_kinetic:
  stage: build
  image: autoware/autoware:bleedingedge-kinetic-base
  variables:
    ROS_DISTRO: kinetic
  <<: *build_common
  script:
    # Install lcov from source as binary installation errors when appending files
    - git clone https://github.com/linux-test-project/lcov.git
    - cd lcov
    - git checkout v1.13
    - sudo make install
    - cd ..
    - rm -r lcov
    # We first build the entire workspace normally
    - source /opt/ros/${ROS_DISTRO}/setup.bash
    - colcon build --cmake-args -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_BUILD_TYPE=Debug
    # And then build the tests target. catkin (ROS1) packages add their tests to the tests target
    # which is not the standard target for CMake projects. We need to trigger the tests target so that
    # tests are built and any fixtures are set up.
    - colcon build --cmake-target tests --cmake-args -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_BUILD_TYPE=Debug
    - lcov --initial --directory build --capture --output-file lcov.base
    - colcon test
    - colcon test-result --verbose
    - lcov --directory build --capture --output-file lcov.test
    - lcov -a lcov.base -a lcov.test -o lcov.total
    - lcov -r lcov.total '*/tests/*' '*/test/*' '*/build/*' '*/devel/*' '/usr/*' '/opt/*' '*/CMakeCCompilerId.c' '*/CMakeCXXCompilerId.cpp' -o lcov.total.filtered
    # BRANCH_NAME: gets branch name from CI_COMMIT_REF_NAME variable substituting / with _ (feature/test_this_lib becomes feature_test_this_lib)
    # CI_COMMIT_REF_NAME: (from https://docs.gitlab.com/ee/ci/variables/) The branch or tag name for which project is built
    - BRANCH_NAME="$(echo $CI_COMMIT_REF_NAME | sed 's/\//_/g')"
    - COVERAGE_FOLDER_NAME="coverage_$BRANCH_NAME"
    - genhtml -p "$PWD" --legend --demangle-cpp lcov.total.filtered -o $COVERAGE_FOLDER_NAME
    - tar -czvf coverage.tar.gz $COVERAGE_FOLDER_NAME
    - ls $CI_PROJECT_DIR
  artifacts:
    paths:
      - $CI_PROJECT_DIR/coverage.tar.gz
    expire_in: 48 hrs
  coverage: /\s*lines.*:\s(\d+\.\d+\%\s\(\d+\sof\s\d+.*\))/

build_melodic:
  stage: build
  image: autoware/autoware:bleedingedge-melodic-base
  variables:
    ROS_DISTRO: melodic
  <<: *build_common
  script:
    # Install lcov from the Ubuntu package. We need 1.13 at least.
    - sudo apt-get install -y lcov
    # We first build the entire workspace normally
    - source /opt/ros/${ROS_DISTRO}/setup.bash
    - colcon build --cmake-args -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_BUILD_TYPE=Debug -DPYTHON_EXECUTABLE=/usr/bin/python3
    # And then build the tests target. catkin (ROS1) packages add their tests to the tests target
    # which is not the standard target for CMake projects. We need to trigger the tests target so that
    # tests are built and any fixtures are set up.
    - colcon build --cmake-target tests --cmake-args -DCMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_C_FLAGS="${CMAKE_C_FLAGS} -fprofile-arcs -ftest-coverage" -DCMAKE_BUILD_TYPE=Debug -DPYTHON_EXECUTABLE=/usr/bin/python3
    - lcov --initial --directory build --capture --output-file lcov.base
    - colcon test
    - colcon test-result --verbose
    - lcov --directory build --capture --output-file lcov.test
    - lcov -a lcov.base -a lcov.test -o lcov.total
    - lcov -r lcov.total '*/tests/*' '*/test/*' '*/build/*' '*/devel/*' '/usr/*' '/opt/*' '*/CMakeCCompilerId.c' '*/CMakeCXXCompilerId.cpp' -o lcov.total.filtered
    # BRANCH_NAME: gets branch name from CI_COMMIT_REF_NAME variable substituting / with _ (feature/test_this_lib becomes feature_test_this_lib)
    # CI_COMMIT_REF_NAME: (from https://docs.gitlab.com/ee/ci/variables/) The branch or tag name for which project is built
    - BRANCH_NAME="$(echo $CI_COMMIT_REF_NAME | sed 's/\//_/g')"
    - COVERAGE_FOLDER_NAME="coverage_$BRANCH_NAME"
    - genhtml -p "$PWD" --legend --demangle-cpp lcov.total.filtered -o $COVERAGE_FOLDER_NAME
    - tar -czvf coverage.tar.gz $COVERAGE_FOLDER_NAME
    - ls $CI_PROJECT_DIR
  artifacts:
    paths:
      - $CI_PROJECT_DIR/coverage.tar.gz
    expire_in: 48 hrs
  coverage: /\s*lines.*:\s(\d+\.\d+\%\s\(\d+\sof\s\d+.*\))/

build_kinetic_cross:
  stage: build
  image: autoware/build:${AUTOWARE_CROSS_TARGET_PLATFORM}-kinetic-${AUTOWARE_DOCKER_DATE}
  variables:
    ROS_DISTRO: kinetic
    <<: *build_cross_vars
  <<: *build_cross_script

build_melodic_cross:
  stage: build
  image: autoware/build:${AUTOWARE_CROSS_TARGET_PLATFORM}-melodic-${AUTOWARE_DOCKER_DATE}
  variables:
    ROS_DISTRO: melodic
    <<: *build_cross_vars
  <<: *build_cross_script

pages:
  stage: deploy
  image: alpine
  dependencies:
    - build_kinetic
    - build_melodic
  script:
    - BRANCH_NAME="$(echo $CI_COMMIT_REF_NAME | sed 's/\//_/g')"
    - COVERAGE_FOLDER_NAME="coverage_$BRANCH_NAME"
    - tar -xzvf coverage.tar.gz
    - mv $COVERAGE_FOLDER_NAME public
  artifacts:
    paths:
      - public
  only:
    - master
